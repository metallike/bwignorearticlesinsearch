<?php

use Shopware\Bundle\SearchBundle\Condition\ProductAttributeCondition;

class Shopware_Plugins_Frontend_BWIgnoreArticlesInSearch_Bootstrap extends Shopware_Components_Plugin_Bootstrap
{
    public function getVersion()
    {
        return '1.0.0';
    }

    public function getLabel()
    {
        return 'BWIgnoreArticlesInSearch';
    }

    public function install()
    {
        $this->subscribeEvent(
            'Shopware_SearchBundle_Create_Search_Criteria',
            'onCreateSearchCriteria'
        );

        $this->subscribeEvent(
            'Shopware_SearchBundle_Create_Ajax_Search_Criteria',
            'onCreateSearchCriteria'
        );

        return true;
    }

    public function onCreateSearchCriteria($args) {
        $criteria = $args->get('criteria');
        $criteria->addCondition(
            // Create the article attribute "my_attr" or rename it
            new ProductAttributeCondition('my_attr', ProductAttributeCondition::OPERATOR_EQ, 1)
        );
    }
}